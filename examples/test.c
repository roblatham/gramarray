#include <stdio.h>
#include "gramarray.h"

void my_callback(int64_t v, void* args) {
	fprintf(stderr,"%c",(char)v);
}

int main() {
	gramarray g;
	gramarray_create(&g, GRAMARRAY_RELATIVE);

	int c = 0;
	char* str = "abcdabababcdabababcdabab";

	while(str[c] != 0) {
		gramarray_append(g, (gramtype)(str[c]));
		c += 1;
	}
	gramarray_print(stdout,g);
	printf("=======\n");

	gramiter it;
	gramarray_iterator_create(g, &it);
	while(1) {
		int64_t v;
		gramarray_iterator_value(it, &v);
		fprintf(stderr,"%c",(char)v);
		if(gramarray_iterator_incr(it) == -1) break;
	}
	fprintf(stderr,"\n");
	gramarray_iterator_free(it);

	// same thing with a callback
	gramarray_iterate(g,my_callback,NULL);
	fprintf(stderr,"\n");

	gramarray_free(g);
	return 0;
}
